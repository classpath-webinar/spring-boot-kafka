## Apache Kafka 
 - Scalable
 - Fault-tolerant
 - Great for publish-subscribe messaging system
 - Throughput in comparison to most messaging systems
 - Highly durable
 - Highly reliable
 - High performance
 